#!/usr/bin/bash
# stop.sh

if [ ! $# -eq 2 ]
then
    echo "Usage: ${0} <domain> <instance-name>"
    exit 1
fi

if [ ! -d "${1}" ]
then
    echo "Cannot find folder ./${1}"
    exit 1
fi

source ${1}/vars.sh
source functions.sh 
remote_script_content=$(cat ${1}/stop-remote.sh)

# Replace variables in stop-remote.sh by their value
for v in ${REMOTE_VARS_PASSTHRU[@]}
do
  remote_script_content=$(sed -e "s#\${${v}}#${!v}#" <<< $remote_script_content)
done

# Execute stop-remote.sh on remote VM
# "StrictHostKeyChecking no" because it's a new server *every time*.
echo "$remote_script_content" | ssh -o 'StrictHostKeyChecking no' -i ${SSH_KEY} ubuntu@${DOMAIN}

echo "  📡 Downloading persistent data to your computer..."
# Get persistent data
# "StrictHostKeyChecking no" because it's a new server *every time*
rsync -av --delete \
  -e="ssh -o 'StrictHostKeyChecking no' -i ${SSH_KEY}" \
  --rsync-path="sudo rsync" \
  ubuntu@${DOMAIN}:${VM_SYNC_FOLDER_PATH}/ ${SYNC_FOLDER_PATH}/

echo   # new line
read -p "  ❓ Delete instance ${1} (${2})? [y/N] " delete_yn
echo   # new line

if [[ $delete_yn =~ [yY].* ]]
then
  echo "  🗑️  Asking for the VM to be deleted..."
  
  nova delete ${2}

  if [[ $? != 0 ]]
  then
    echo "  ❌ Could not delete VM."
    exit 1
  fi
  
  echo "  ⏳ Waiting for VM to be deleted..."
  
  while nova list | grep "${2}" > /dev/null
  do
    sleep 5
    echo "  Waiting..."
  done
  echo "  ✅ $(tput setaf 2)VM Deleted! $(tput setaf 7)"

  echo "  📍 Setting DNS to 0.0.0.0 ..."
  # Set DNS to 0.0.0.0
  dyndns_result="$(update_dyn_dns ${DYNDNS_USER} ${DYNDNS_PASSWORD} ${DOMAIN} 0.0.0.0)"

  if [[ $dyndns_result == 0 ]]
  then
    echo "  ✅ $(tput setaf 2)DNS updated! $(tput setaf 7)"
  else
    echo "  ❌ $(tput setaf 1)DNS not updated!"
    echo "     $dyndns_result $(tput setaf 7)"
  fi
else
  echo "  VM was not deleted!"
fi

nova list
echo    # newline
echo "  🥁 All done!"
echo    # newline