#!/usr/bin/bash
# setup-remote.sh
sudo apt install --assume-yes vim certbot
cd ${VM_SYNC_FOLDER_PATH}
# Put letsencrypt config back in place
sudo tar -xvf letsencrypt.tar.gz
sudo rm -rf /etc/letsencrypt
sudo mv etc/letsencrypt /etc/
sudo rm -rf etc
# Request certificate from letsencrypt
sudo certbot certonly --standalone --domain ${DOMAIN} --register-unsafely-without-email --non-interactive --agree-tos

