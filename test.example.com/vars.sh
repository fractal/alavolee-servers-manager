INSTANCE_NAME="example-$(date +"%Y%m%d-%H%M%S")"
INSTANCE_FLAVOR="91ca682c-3d92-4a8d-afde-a88d00b7f21e"
INSTANCE_IMAGE="e9f68b2d-3a89-45fe-a4f7-1f2475a2865f"
DOMAIN="test.example.com"

SSH_KEY="/home/user/.ssh/id_rsa"

DYNDNS_USER="dnsuser"
DYNDNS_PASSWORD="m0nk3y"

# Rsynced with the remote vm's VM_SYNC_FOLDER_PATH directory
SYNC_FOLDER_PATH="${DOMAIN}/persistence"

# Rsynced with SYNC_FOLDER_PATH.
VM_SYNC_FOLDER_PATH="/home/ubuntu/persistence"

# Variables which need to be available to setup-remote.sh or stop-remote.sh
REMOTE_VARS_PASSTHRU=()


# https://docs.ovh.com/gb/en/public-cloud/set-openstack-environment-variables/
source ~/openrc.sh
