#!/usr/bin/bash
# stop-remote.sh

cd ${VM_SYNC_FOLDER_PATH}

echo "💾 Saving letsencrypt configuration..."
sudo tar -czvf ./letsencrypt.tar.gz /etc/letsencrypt
sudo chown ubuntu:ubuntu ./letsencrypt.tar.gz
