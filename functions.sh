#!/usr/bin/bash

function check_dependencies() {
    deps=(nova curl dig sudo ssh ssh-keygen rsync cat sed)

    echo "  Checking dependencies..."
    for i in "${deps[@]}"
    do
        dep_path="$(which ${i})"
        if [ "$dep_path" = "" ]
        then
            echo "  ❌ ${i} not found."
            exit 1
        fi
    done
}

function update_dyn_dns() {
    # ${DYNDNS_USER} ${DYNDNS_PASSWORD} ${DOMAIN} ${INSTANCE_IP}
    dyndns_result="$(curl -s -m 5 -L --location-trusted \
       --user "${1}:${2}" \
       "https://www.ovh.com/nic/update?system=dyndns&hostname=${3}&myip=${4}")"
    
    if [[ $dyndns_result == good* ]] || [[ $dyndns_result == nochg* ]]
    then
        echo "0"
    else
        echo "${dyndns_result}"
    fi
}