#!/usr/bin/bash
# setup.sh

source functions.sh

# Check if a parameter was supplied to the script
if [ $# -eq 0 ]
then
    echo "Usage: ${0} <domain>"
    exit 1
fi

# Check if the parameter is the name of a subfolder
if [ ! -d "${1}" ]
then
    echo "Cannot find folder ./${1}"
    exit 1
fi

# Get variables
source "${1}/vars.sh"

# Check dependencies
# see functions.js
check_dependencies

timestamp_start="$(date +"%s")"

# These commands are used to change the ouput color
echo "$(tput setaf 7)"
echo "  ➕ Creating VM."
# Create a new VM using Openstack's nova client
nova_boot_res=$(nova boot --key-name ovh_games \
  --flavor "${INSTANCE_FLAVOR}" \
  --image "${INSTANCE_IMAGE}" \
  "${INSTANCE_NAME}")

if [[ $? != 0 ]]
then
  echo "  ❌ Could not create VM."
  exit 1
fi

INSTANCE_IP=''

echo "  ⏳ Waiting for the VM to be ready..."
# Wait until VM is ready and has an IPv4 address
until [[ $INSTANCE_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]
do
	INSTANCE_IP=$(nova show $INSTANCE_NAME | grep network | cut -d "|" -f 3 | cut -d "," -f 1 | tr -d " ")
  if [[ $INSTANCE_IP == *:* ]]; then
    INSTANCE_IP=$(nova show $INSTANCE_NAME | grep network | cut -d "|" -f 3 | cut -d "," -f 2 | tr -d " ")
  fi
	echo "     Waiting..." 
	sleep 10
done

echo "  ✅ $(tput setaf 2)VM is ready, with IP ${INSTANCE_IP}$(tput setaf 7)"

echo "  📍 Setting dynDNS for ${DOMAIN} to ${INSTANCE_IP}..."

dyndns_result="$(update_dyn_dns ${DYNDNS_USER} ${DYNDNS_PASSWORD} ${DOMAIN} ${INSTANCE_IP})"

if [[ $dyndns_result == 0 ]]
then
  echo "  ✅ $(tput setaf 2)DNS updated! $(tput setaf 7)"
else
  echo "  ❌ $(tput setaf 1)DNS not updated!"
  echo "$dyndns_result $(tput setaf 7)"
  exit 1
fi

# Regularly check DNS until it answers with the new IP we just set
echo "  ⏳ Waiting for the DNS update to be live..."
until [[ $INSTANCE_IP = $(dig +short ${DOMAIN}) ]]
do
	sleep 10
  echo "     Waiting..."	
done

echo "  ✅ $(tput setaf 2)DNS record is available! $(tput setaf 7)${DOMAIN}"

echo "  📡 Uploading persistent data to your server..."

# remove old fingerprint
ssh-keygen -f ~/.ssh/known_hosts -R "${DOMAIN}"
# copy persistent data to server
# "StrictHostKeyChecking no" because it's a new server *every time*
rsync -e "ssh -o 'StrictHostKeyChecking no' -i ${SSH_KEY}" \
-av ${SYNC_FOLDER_PATH}/ ubuntu@${DOMAIN}:${VM_SYNC_FOLDER_PATH}/


# Replace variables in the remote commands script by their value
remote_script_content=$(cat ${1}/setup-remote.sh)
for v in ${REMOTE_VARS_PASSTHRU[@]}
do
  remote_script_content=$(sed  -e "s#\${${v}}#${!v}#" <<< $remote_script_content)
done

echo "  📞 Executing remote script ..."
# configure server
# "StrictHostKeyChecking no" because it's a new server *every time*.
echo "$remote_script_content" | ssh -o 'StrictHostKeyChecking no' -i ${SSH_KEY} ubuntu@${DOMAIN}

timestamp_end="$(date +"%s")"

timestamp_diff=$((timestamp_end-timestamp_start)) 

nova list

echo
echo "  🥁 All done! Instance name is: ${INSTANCE_NAME}"
echo "     Creating and provisioning the server took ${timestamp_diff} seconds."
echo 
echo "     When you're finished with this server, please run :"
echo 
echo "  ➜ $(tput setaf 2)bash stop.sh ${DOMAIN} ${INSTANCE_NAME}$(tput setaf 7)"
echo 